// VARS =============================================================
const billTotalInput = document.getElementById('bill-total-input')
const percentSelectorBtn = document.querySelectorAll('.percent-selector-btn')
const customTipInput = document.getElementById('custom-tip-input')
const totalPersonInput = document.getElementById('total-person-input')
const calcBtn = document.getElementById('calc-btn')
const totalBillAmtPerpPerson = document.querySelector('.total-bill-amt-per-person')
const totalTipAmtPerpPerson = document.querySelector('.total-tip-amt-per-person')

const state = {
    billTotalInput: '',
    customTipInput: '',
    percentSelectorBtn: '',
    totalPersonInput: '',
    totalTipPerPerson: '',
    totalAmountPerPerson: '',
    inputElements: [billTotalInput, customTipInput, totalPersonInput]
}

// FUNCTIONS =========================================================
const updateState = (text, input) => {
    state[input] = text
}

const covertToNum = (val) => {
    return parseFloat(val)
}

const getPercentVal = (str) => {
    state.percentSelectorBtn = str
}

const calculateTotalAmt = () => {
    let billAmt = parseFloat(state.billTotalInput).toFixed(2)
    billAmt = parseFloat(billAmt)
    let tipPercent = state.percentSelectorBtn || state.customTipInput
    tipPercent = parseFloat(tipPercent).toFixed(2)
    tipPercent = parseFloat(tipPercent)
    let numPeople = state.totalPersonInput
    let tipAmtPerPerson = (billAmt * tipPercent) / numPeople;
    let totalPerPerson = (billAmt / numPeople) + tipAmtPerPerson;
    state.totalTipPerPerson = tipAmtPerPerson.toFixed(2)
    state.totalAmountPerPerson = totalPerPerson.toFixed(2)
    totalTipAmtPerpPerson.textContent = `$${state.totalTipPerPerson}`
    totalBillAmtPerpPerson.textContent = `$${state.totalAmountPerPerson}`
}

const emptyCheck = () => {
    if(state.customTipInput === '') {
        return true
    }
    if(state.billTotalInput === '' || state.percentSelectorBtn === '' || state.totalPersonInput === '') {
        return true
    } else {
        return false
    }
}

const addErrors = (inputElements) => {
    inputElements.forEach(elm => {
        if(elm.value === '') {
            elm.classList.add('error')
            let span = elm.parentNode.previousElementSibling.firstElementChild.firstElementChild
            span.style.opacity = '1'
        }
    })
    percentSelectorBtn.forEach(btn => {
        if(btn.classList.contains('active')) {
            let span = btn.parentNode.previousElementSibling.firstElementChild.firstElementChild
            span.style.opacity = '0'
        }
    })
}

const removeErrors = (elm) => {
    let span = elm.parentNode.previousElementSibling.firstElementChild.firstElementChild
    span.style.opacity = '0'
    elm.classList.remove('error')
}


// EVENT LISTENERS ===================
calcBtn.addEventListener('click', (e) => {
    const isEmpty =  emptyCheck()
    if(isEmpty) {
        addErrors(state.inputElements)
    } else {
        calculateTotalAmt()
    }
})

percentSelectorBtn.forEach((btn) => {
    let span = percentSelectorBtn[0].parentNode.previousElementSibling.firstElementChild.firstElementChild
    span.style.opacity = '0'
    btn.addEventListener('click', (e) => {
        let span = percentSelectorBtn[0].parentNode.previousElementSibling.firstElementChild.firstElementChild
        span.style.opacity = '0'
        percentSelectorBtn.forEach((active) => {
            active.classList.remove('active');
        });
        e.target.classList.add('active')
        const dataPercent = e.target.dataset.percent;
        customTipInput.value = ''
        customTipInput.placeholder = 'Custom'
        state.customTipInput = 0
        getPercentVal(dataPercent)
    })
});


billTotalInput.addEventListener('keyup', (e) => {
    removeErrors(billTotalInput)
    let val = covertToNum(e.target.value).toFixed(2)
    updateState(val, e.target['name'])
})

customTipInput.addEventListener('focus', (e) => {
    state.percentSelectorBtn = 0
    percentSelectorBtn.forEach((active) => {
        active.classList.remove('active');
    });
})

customTipInput.addEventListener('keyup', (e) => {
    removeErrors(customTipInput)
    percentSelectorBtn.forEach((active) => {
        active.classList.remove('active');
    });
    let val = parseInt(e.target.value)
    updateState(val / 100, e.target['name'])
})

totalPersonInput.addEventListener('keyup', (e) => {
    removeErrors(totalPersonInput)
    const val = parseInt(e.target.value)
    updateState(val, e.target['name'])
})
totalPersonInput.addEventListener('change', (e) => {
    removeErrors(totalPersonInput)
    const val = parseInt(e.target.value)
    updateState(val, e.target['name'])
})
